#include "stdafx.h"
#include "math.h"
#include <iostream> 
#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>

#include "coMatrix.h"
#define MY_DEBUG	0
/*#include "eigen-3.4.0/Eigen/Core"*/
#include <Eigen/Dense>
using namespace Eigen;
using namespace std;

void ClearList_eff_frontier();
int Read_universal(char* sFileName, StringVec*pVu_name, DoubleVec*pVu_r, DoubleVec*pVu_std);
int Read_correlation(char* sFileName, int nSize, DoubleMatarix*pMu_cor);
double line_solver(unsigned int nDim, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, MatrixXd *A, Vector2d* b);
double Loss(VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, MatrixXd *A, Vector2d* b);
double Loss_line(int N, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, MatrixXd *A, Vector2d* b);
double gradient_descent(unsigned long maxStep, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, double eta1, double eta2, double eps, MatrixXd *A, Vector2d* b);
double gradient_descent2(unsigned long maxStep, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, double eta1, double eta2, double eps, MatrixXd *A, Vector2d* b);

StringVec Vu_name;
DoubleVec Vu_r;
DoubleVec Vu_std;
DoubleMatarix Mu_cor;
StringVec g_ErrorMsg;

int main(int argc, char**argv)
{
	int nRet = 0;
	int N = 0;
	int isOption_r = 0;
	char f_universe[1024];
	char f_correlation[1024];
	if (argc < 2) {
		cout << "Error Arguments";
		return -1;
	}
	int i1, i2;
	int i_c=0;
	if (argc >= 3) {
		for (int i = 1;i < argc;i++) {
			if (argv[i][0] == '-' && argv[i][1] == 'r') {
				i_c = i;
				isOption_r = 1;
			}
		}
	}
	switch (i_c) {
	case 0:
		i1 = 1;
		i2 = 2;
		break;
	case 1:
		i1 = 2;
		i2 = 3;
		break;
	case 2:
		i1 = 1;
		i2 = 3;
		break;
	case 3:
		i1 = 1;
		i2 = 2;
		break;
	}
	memset(f_universe, 0, 1024);
	memset(f_correlation, 0, 1024);
	
	g_ErrorMsg.push_back("");
	g_ErrorMsg.push_back("");
	g_ErrorMsg.push_back("Is there 2 files in the arguments list?");
	g_ErrorMsg.push_back(",this file is not exists!");
	g_ErrorMsg.push_back("Please check is there each of the N rows have N numeric values!");
	g_ErrorMsg.push_back("The asset names is empty!");
	g_ErrorMsg.push_back("Please sure that each input that should be numeric is numeric!");
	if (argv[i1] == NULL) {
		cout << "\r\nError:" << g_ErrorMsg[2].c_str();
		return -1;
	}
	else {
		strncpy (f_universe, argv[i1], 1024-1);	
		//strcpy_s(f_universe, 1024, argv[i1]);
		nRet = Read_universal(f_universe, &Vu_name, &Vu_r, &Vu_std);
		if (nRet < 0) {
			string s = g_ErrorMsg[-nRet];
			//s.replace("%s", f_correlation);
			cout << "\r\nError:" << f_universe << " " << s.c_str();
			return -1;
		}
		else
			N = nRet;
	}

	if (argv[i2] == NULL) {
		cout << "\r\nError:" << g_ErrorMsg[2].c_str();
		return -1;
	}
	else {
		strncpy (f_correlation, argv[i2], 1024-1);	
		//strcpy_s(f_correlation, 1024, argv[i2]);
		nRet = Read_correlation(f_correlation, N, &Mu_cor);
		if (nRet < 0) {
			string s = g_ErrorMsg[-nRet];
			//s.replace("%s", f_correlation);
			cout << "\r\nError:" << f_correlation << " "<< s.c_str();
			return -1;
		}

	}
	
	
	if (Vu_name.size()!=Vu_r.size() || Vu_std.size()!=Mu_cor.size()){
		cout << "\r\nInconsistent Data Dimesions! ";
		return EXIT_FAILURE;
	}

	VectorXd x(N);
	Vector2d Lumba;
	Vector2d B;
	x.setRandom();
	Lumba.setRandom();
	
	MatrixXd cov(MatrixXd::Identity(N, N));
	MatrixXd sigm(MatrixXd::Identity(N, N));
	MatrixXd A(MatrixXd::Ones(2,N));
	for (std::vector<std::vector<double> >::size_type i = 0;i < Mu_cor.size();i++) {
		A(1, i) = Vu_r[i];
	}
	
	for (std::vector<std::vector<double> >::size_type i = 0;i < Mu_cor.size();i++) {
		DoubleVec r = Mu_cor[i];  //Mu_cor
		for (std::vector<std::vector<double> >::size_type j = 0;j < Mu_cor.size();j++)
			sigm(i, j) = r[j];
	}

	for (std::vector<std::vector<double> >::size_type i = 0;i < Mu_cor.size();i++) {
		for (std::vector<std::vector<double> >::size_type j = 0;j < Mu_cor.size();j++)
			cov(i, j) = sigm(i, j)*Vu_std[i]* Vu_std[j];
	}
	double eta = 0.006;
	double eps = 0.000001;
	double L;
	//double L2;
	int k = 0;
	cout << "ROR,volatility" << endl;
	while (k < 26) {
		B << 1, ((k+1)*0.01);
		cout << fixed << setprecision(1) << 100 * B(1) << "%,";
		if (isOption_r==1)
			L = gradient_descent2(10000000, &x, &Lumba, &cov, eta, eta, eps, &A, &B);
		else
			L = line_solver(N, &x, &Lumba, &cov, &A, &B);
		cout << fixed << setprecision(2) << L*100 << "%" << endl;// << L2 << "\t" << endl; //<<L2-L<<
		k++;
	}
	ClearList_eff_frontier();
}

//CX=b
double line_solver(unsigned int nDim, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, MatrixXd *A, Vector2d* b)
{
	VectorXd X(nDim+2);
	VectorXd B(nDim + 2);
	int N = nDim;
	for (unsigned int i = 0;i < nDim;i++)
		X(i) = (*x)(i);
	for (unsigned int i = 0;i < 2;i++)
		X(i+ nDim) = (*Lumba)(i);

	MatrixXd C(MatrixXd::Zero(N+2, N+2));
	for (unsigned int i = 0;i < nDim;i++) {
		for (unsigned int j = 0;j < nDim;j++)
			C(i, j) = (*sigm)(i, j);
		C(i, nDim) = (*A)(0,i);
		C(i, nDim+1) = (*A)(1,i);
	}

	{
		for (unsigned int j = 0;j < nDim;j++) {
			C(nDim, j) = (*A)(0,j);
			C(nDim+1, j) = (*A)(1, j);
		}
	}
	{
		for (unsigned int j = 0;j < nDim;j++) {
			B(j) = 0;
			
		}
		B(nDim ) = 1;
		B(nDim + 1) = (*b)(1);
	}
	X = C.colPivHouseholderQr().solve(B);
	double L = Loss_line(N, &X, Lumba, sigm, A, b);
	if (MY_DEBUG == 1) {
		cout << "X[N+2]=" << X << endl;		
		cout << "L=" << L << endl;
	}
	return L;
}
//use gradient_descent method to solve 
double gradient_descent(unsigned long maxStep, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, double eta1, double eta2, double eps, MatrixXd *A, Vector2d* b)
{
	VectorXd X(*x);
	VectorXd dX(X);
	Vector2d lumba(*Lumba);
	Vector2d d_lumba(lumba);
	unsigned long k = 0;
	int do1 = 0;
	int do2 = 0;
	while (k < maxStep) {
		Vector2d lumba0(lumba);
		VectorXd X0(X);
		VectorXd dL_dx = (*sigm)*X0 + A->transpose()*lumba0;
		X = X0 - eta1*dL_dx;
		Vector2d dL_dl = (*A)*X0 - (*b);
		lumba = lumba0 + eta2*dL_dl;
		dX = X - X0;
		d_lumba = lumba - lumba0;
		if (dL_dx.norm() < eps && dL_dl.norm() < eps){
			break;
		}
		
		if (dX.norm() < 10 * eps && do1==0) {
			eta1 = eta1 / 3;
			do1 = 1;
		}
		if (d_lumba.norm() < 10 * eps && do2==0) {
			eta2 = eta2/ 3;
			do2 = 1;
		}

		if (k % 2000 == 0 && 1==0) {
			cout << "X[" << k << "]=" << X << endl;
			cout << X << endl;
		}
		k++;
	}
	//cout << "  Stop at step " << k << "\t";
	*x = X;
	*Lumba = lumba;
	double L = Loss(x, Lumba, sigm, A, b);
	if (MY_DEBUG==1) {
		cout << "Stop in step " << k << endl;
		cout << "X[" << k << "]=" << *x << endl;
		cout << "L=" << L << endl;
	}
	return L;
}

double gradient_descent2(unsigned long maxStep, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, double eta1, double eta2, double eps, MatrixXd *A, Vector2d* b)
{
	VectorXd X(*x);
	VectorXd dX(X);
	Vector2d lumba(*Lumba);
	Vector2d d_lumba(lumba);
	unsigned long k = 0;
	int do1 = 0;
	int do2 = 0;
	while (k < maxStep) {
		Vector2d lumba0(lumba);
		VectorXd X0(X);
		VectorXd dL_dx = (*sigm)*X0 + A->transpose()*lumba0;
		X = X0 - eta1*dL_dx;
		for (std::vector<double>::size_type i = 0;i < Vu_r.size();i++){ //X.NumDimensions;i++) {
			if (X(i) < 0)
				X(i) = 0;
		}
		Vector2d dL_dl = (*A)*X0 - (*b);
		lumba = lumba0 + eta2*dL_dl;
		dX = X - X0;
		d_lumba = lumba - lumba0;
		if (dL_dx.norm() < eps && dL_dl.norm() < eps) {
			break;
		}
		
		if (dX.norm() < 10 * eps && do1 == 0) {
			eta1 = eta1 / 3;
			do1 = 1;
		}
		if (d_lumba.norm() < 10 * eps && do2 == 2) {
			eta2 = eta2 / 3;
			do2 = 1;
		}
		
		if (k % 2000 == 0 && 1 == 0) {
			cout << "X[" << k << "]=" << X << endl;
			cout << X << endl;
		}
		k++;
	}
	
	*x = X;
	*Lumba = lumba;
	double L = Loss(x, Lumba, sigm, A, b);
	//cout << "  Stop at step " << k << "\t";
	//cout << "X[" << k << "]=" << *x <<endl;
	if (MY_DEBUG == 1) {
		cout << "Stop in step " << k << endl;
		cout << "X[" << k << "]=" << *x << endl;
		cout << "L=" << L << endl;
	}
	return L;
}

double Loss(VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, MatrixXd *A, Vector2d* b)
{
	double d = 0;
	d = (x->transpose())*(*sigm)*(*x);
	d = sqrt(d) ;//+(Lumba->transpose())*((*A)*(*x) - (*b));

	return d;
}
double Loss_line(int nDim, VectorXd* x, Vector2d* Lumba, MatrixXd *sigm, MatrixXd *A, Vector2d* b)
{
	int N = nDim;
	VectorXd X(N);
	for (int i = 0;i < N;i++)
		X(i) = (*x)(i);

	double d = 0;
	d = (X.transpose())*(*sigm)*X;
	d = sqrt(d);
	//d = Loss(&X, Lumba, sigm, A, b);
	return d;
}
void ClearList_eff_frontier() {
	g_ErrorMsg.clear();
	Vu_name.clear();
	Vu_r.clear();
	Vu_std.clear();
	ClearMatrix(&Mu_cor);
}

