#include "stdafx.h"
#include "coMatrix.h"
#define DO_CALC_COV 0  // is calculate corr[i,i] or just set =1
StrVect vecColList;
DoubleMatarix g_mtrAssets;
DoubleMatarix g_mtrRate;
DoubleVec g_avg_rate;
DoubleVec g_std_rate;
DoubleMatarix g_Covariance;
DoubleMatarix g_Correlation;

//calu_Rate(&g_mtrAssets, &g_mtrRate);
void initMatrix(DoubleMatarix*pM, int nRows, int nCols)
{
	ClearMatrix(pM);
	double v = 0.0;
	for (int i = 0;i < nRows ;i++) {
		DoubleVec rateItems;
		for (int j = 0;j < nCols;j++) {
			rateItems.push_back(v);
		}
		pM->push_back(rateItems);
	}
}
void calu_Rate(DoubleMatarix* pAsset, DoubleMatarix* pRate)
{
	int rows = pAsset->size();
	int cols;
	if (rows > 0) {
		cols = (*pAsset)[0].size();
	}
	else
		return;
	double v;
	int i, j;
	i = 0;
	//DoubleVec rateItems;
	/*for (j = 0;j < cols;j++) {
		v = 0.0;
		rateItems.push_back(v);
	}
	pRate->push_back(rateItems);
	*/
	for ( i = 1;i < rows;i++) {
		DoubleVec rateItems;
		for ( j = 0;j < cols;j++) {
			//cout << i << "," << j <<"\t";
			if ((*pAsset)[i - 1][j] != 0)
				v = (*pAsset)[i][j] / (*pAsset)[i - 1][j] - 1.0;
			else
				v = 999.99;
			rateItems.push_back(v);
		}
		pRate->push_back(rateItems);
		//cout << endl;
	}
}
//compute the average return and standard deviation for that "asset", [rate of return]
void calu_AvgStd(DoubleMatarix* pRate, DoubleVec* pAvgRate, DoubleVec* pStdRate)
{
	int rows = pRate->size();
	int cols;
	if (rows > 0) {
		cols = (*pRate)[0].size();
	}
	else
		return;
	double v;
	double avg_v;
	pAvgRate->clear();
	pStdRate->clear();
	for (int j = 0;j < cols;j++) {
		v = 0.0;
		for (int i = 0;i < rows;i++) {
			v += (*pRate)[i][j];
		}
		v = v / rows;
		pAvgRate->push_back(v);
	}
	for (int j = 0;j < cols;j++) {
		v = 0.0;
		avg_v = (*pAvgRate)[j];
		for (int i = 0;i < rows;i++) {
			v += (((*pRate)[i][j]- avg_v)*((*pRate)[i][j] - avg_v));
		}
		v = sqrt(v / (rows-1)); //some n-1
		pStdRate->push_back(v);
	}
}
//covariance matrix
/*
        1
s_ab = --- Sum_t (ra_t - ra_avg)(rb_t - rb_avg)
        n

Cov = [s_ab]
*/
void calu_Covariance(DoubleMatarix* pRate, DoubleVec* pAvgRate, DoubleMatarix* pCov)
{
	int rows = pRate->size();
	int cols;
	if (rows > 0) {
		cols = (*pRate)[0].size();
	}
	else
		return;
	initMatrix(pCov, rows, cols);
	double v;
	double avg_i;
	double avg_j;
	//To form a [cols X cols] matrix
	for (int i = 0;i < cols;i++) {
		for (int j = 0;j < cols;j++) {
			v = 0.0;
			avg_i = 1.0*(*pAvgRate)[i];
			avg_j = 1.0*(*pAvgRate)[j];
			for (int k = 0;k < rows;k++)
				v =v+ (((*pRate)[k][i]- avg_i)*((*pRate)[k][j] - avg_j)); //sum_t(ra_t - ra_avg)(rb_t - rb_avg)
			(*pCov)[j][i] = v/(rows); //some say it should be (rows-1)
		}
	}
}
/*      s_ab
p_ab = -------
       s_a s_b
Cor = [s_ab/s_a/s_b]
*/

void calu_Correlation(DoubleMatarix* pCov, DoubleVec* pStdRate, DoubleMatarix* pCor)
{
	int rows = pCov->size();
	int cols;
	if (rows > 0) {
		cols = (*pCov)[0].size();
	}
	else
		return;
	initMatrix(pCor, cols, cols);
	double v;
	double s_a;
	double s_b;
	for (int a = 0;a < cols;a++) {
		for (int b = 0;b < cols;b++) {			
			if (a != b || DO_CALC_COV ==1) {
				v = (*pCov)[a][b];
				s_a = (*pStdRate)[a];
				s_b = (*pStdRate)[b];
				(*pCor)[a][b] = v / (s_a*s_b); 
			}
			else
				(*pCor)[a][b] = 1.0;
		}
	}
}
void ShowMatrix(DoubleMatarix *pM, int nPrecision)
{
	int rows = pM->size();
	int cols;
	if (rows > 0) {
		cols = (*pM)[0].size();
	}
	else
		return;
	int j;
	double d;
	for (int i = 0;i < rows;i++) {
		for ( j = 0;j < cols-1;j++) {
			d = (*pM)[i][j];// +0.00005;
			//printf("%.4lf,", d);cout.widen(nPrecision);			
			//cout << fixed << setprecision(nPrecision)<< setw(nPrecision+5) << right << d  << ",";
			if(i>0 && j==0)
				cout <<" ";
			cout << fixed << setprecision(nPrecision) << setw(nPrecision +3) << right << d << ",";
			//printf("%.4lf,", d);
			
		}
		d = (*pM)[i][j];//+0.00005;
		//printf("%.4lf", d);
		cout << fixed << setprecision(nPrecision) << setw(nPrecision+3) << right << d  ;
		if(i<rows-1)
			cout << endl;
	}
}
void ShowVector(DoubleVec *pV,int nPrecision)
{
	int n = pV->size();
	if (n==0)
		return;
	for (int i = 0;i < n;i++) {
		{
			cout << fixed << setprecision(nPrecision) << setw(nPrecision) << left << (*pV)[i] << "\t,";
		}	
	}
	cout << endl;
}
void ClearMatrix(DoubleMatarix *pM)
{
	int rows =pM->size();
	for (int i = 0;i < rows;i++)
		(*pM)[i].clear();
	(*pM).clear();
}
void ClearList_Matrix() {
	vecColList.clear();
	ClearMatrix(&g_mtrAssets);
	ClearMatrix(&g_mtrRate);
	ClearMatrix(&g_Covariance);
	ClearMatrix(&g_Correlation);
}

