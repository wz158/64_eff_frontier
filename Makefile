src=$(wildcard ./*.cpp)
obj=$(patsubst ./%.cpp, ./%.o, $(src))
target=efficient_frontier
INCLUDE_DIR =eigen-3.4.0/
cc = g++
CPPFLAGS = -std=c++11 -Wall -Werror -pedantic
$(target):$(obj)
	$(cc)	$(obj) -o $(target)

%.o:%.cpp
	$(cc)	-c $< -o $@ $(CPPFLAGS)

.PHONY:clean 
clean:
	rm $(obj)	$(target) -f
 
hello:
	echo "Hello,makefile"

