#include "stdafx.h"
#include "coMatrix.h"
#define ISHEAD 0
//load asset from csv,store in some Variables:vecColList,g_mtrAssets
 std::string& Mytrim(std::string &s) 
 {
     if (s.empty()) 
     {
         return s;
     }
	 if(debug)
		 cout<<s.c_str();
     s.erase(0,s.find_first_not_of(" "));
     s.erase(s.find_last_not_of(" ") + 1);
	 s.erase(0,s.find_first_not_of("\t"));
     s.erase(s.find_last_not_of("\t") + 1);
	 if(debug)
		 cout<<" change to "<< s.c_str() <<endl;
     return s;
 }
int ReadAsset(char* sFileName)
{
	ifstream _csvInput(sFileName, ios::in);
	if (!_csvInput.is_open())
		return -3;	//file not exists
	//char ch; _csvInput >> ch;
	char buf[1024];
	int n = 1;
	while (n > 0 && ISHEAD==1) {
		memset(buf, 0, 1024);
		_csvInput.getline(buf, 1024);// >> buf;
		n = strlen(buf);
	}
	//after a Zone bytes line,serach a line contain ','
	
	char *pChar=NULL;
	if (ISHEAD == 1) {//skip Head
		do {
			memset(buf, 0, 1024);
			_csvInput.getline(buf, 1024);
			pChar = strchr(buf, ',');
		} while (pChar == NULL);
	}
	else
	{
		memset(buf, 0, 1024);
		_csvInput.getline(buf, 1024);
	}
	//here is columns list,get the assets list to vecColList	
	//printf("%s\r\n", buf);
	string s(buf);
	int i, i0, i1;
	i = s.find(',');
	if (i >= 0) {
		i0 = s.rfind(' ', i);
		string colName = s.substr(i0 + 1, i - i0 - 1);
		vecColList.push_back(colName);
		i1 = s.find(',', i + 1);
		while (i1 >= 0) {
			if (i1 >= 0 && i1 > i) {
				colName = s.substr(i + 1, i1 - i - 1);
				colName=Mytrim(colName);
				//cout << colName<< endl;
				vecColList.push_back(colName);
			}
			i = i1;
			i1 = s.find(',', i + 1);
		}
		i1 = s.find('\\', i + 1);
		if (i1 < 0)
			i1 = s.length();
		if (i1 >= 0 && i1 > i) {
			colName = s.substr(i + 1, i1 - i - 1);
			colName=Mytrim(colName);
			//cout << colName<< endl;
			vecColList.push_back(colName);
		}
		//output the column list
		for (long unsigned int i = 1;i < vecColList.size();i++) {
			cout << vecColList[i].c_str() << endl;			
			//printf("%s\r\n",vecColList[i].c_str());
		}
	}

	//begin get data here

	string colVal;
	double fv;
	int nCol = 0;
	int nRow = 0;
	do {
		memset(buf, 0, 1024);
		_csvInput.getline(buf, 1024); //_csvInput >> buf;
		n = strlen(buf);
		//printf("%s\r\n", buf);
		if (n > 0) {
			DoubleVec assetItems;
			string s(buf);
			i0 = 0;
			i = s.find(',');
			nCol = 0;
			while (i >= 0) {
				colVal = s.substr(i0, i - i0);
				if (nCol == 0) {
					int j = colVal.find('-', 0);
					while (j > 0) {
						colVal = colVal.replace(j, 1, 0, '0');
						j = colVal.find('-', j);
					}
				}
				if (colVal.length() == 0 || strcmp(colVal.c_str(), "null") == 0 || strcmp(colVal.c_str(), "NULL") == 0
					//colVal.compare("null") == 0 ||colVal.compare("NULL")
					) 
				{
					if (nRow > 0)
						fv = g_mtrAssets[nRow - 1][nCol - 1];	//for column 0 is ignored,so do -1,using last value for null
					else {
						cout << "Some Value is NULL at row 1!" << endl << s.c_str() << endl;
						cout << "colVal=" << colVal.c_str() << endl;
						perror("Error !");
						exit(1);
					}
				}
				else
					fv = atof(colVal.c_str());
				if (nCol > 0)	//here ignor the date/time colnum
					assetItems.push_back(fv);
				i0 = i + 1;
				i = s.find(',', i0);
				nCol++;
			}
			i = s.find('\\', i0);
			if (i < 0)
				i = s.find('}', i0);
			if (i< 0)
				i = s.length();
			if (i >= 0) {
				colVal = s.substr(i0, i - i0);
				if (colVal.length() == 0 || strcmp(colVal.c_str(), "null") == 0 || strcmp(colVal.c_str(), "NULL") == 0
					//colVal.compare("null") == 0 ||colVal.compare("NULL")
					)
				{
					if (nRow > 0)
						fv = g_mtrAssets[nRow - 1][nCol - 1];  //for column 0 is ignored,so do -1,using last value for null
					else
						cout << "Some Value is NULL at row 1!" << endl;
				}
				else
					fv = atof(colVal.c_str());
				assetItems.push_back(fv);
			}
			g_mtrAssets.push_back(assetItems);
		}
		nRow++;
	} while (n > 0);

	//getline(_csvInput, line);

	return 0;
}

//get the return rate and std
int Read_universal(char* sFileName, StringVec*pVu_name, DoubleVec*pVu_r, DoubleVec*pVu_std)
{
	ifstream _csvInput(sFileName, ios::in);
	if (!_csvInput.is_open())
		return -3;	//The file is not exists
	pVu_name->clear();
	pVu_r->clear();
	pVu_std->clear();
	char buf[1024];
	char buf2[128];
	int n = 1;
	int ret = 0;
	while (n > 0) {
		if (ret < 0) {
			cout << "Error occur while reading "<< sFileName<<" at:" << buf;
			break;
		}
		memset(buf, 0, 1024);
		_csvInput.getline(buf, 1024);// >> buf;
		n = strlen(buf);
		int i, i0;
		if (n > 0) {
			string s(buf);
			i0 = 0;
			i = s.find(',',i0);	//get universal name
			if (i > 0) {
				string s1 = s.substr(i0, i - i0);
				s1 = Mytrim(s1);
				if (s1.length() > 0)
					pVu_name->push_back(s1);
				else
					ret = -5;
			}
			else {
				ret = -4;
			}

			i0 = i + 1;		//get universal rate of year
			i = s.find(',',i0);
			if (i > 0) {
				string s1 = s.substr(i0, i - i0);
				s1 = Mytrim(s1);
				if (s1.length() > 0) {
					try {
						char * end;
						double d = std::strtod(s1.c_str(), &end);
						memset(buf2, 0, 128);
						sprintf(buf2, "%f", d);
						string s(buf2);
						if (s.compare(s1.c_str())==0)
							pVu_r->push_back(d);
						else
							ret = -6;
					}
					catch (std::exception& e) {
						cout << e.what();
						ret = -6;
					}
				}
				else
					ret = -4;
			}

			i0 = i + 1;		//get universal std of year
			i = s.find(',',i0);
			//if (i > 0)
			{//the last one
				i = n;
				string s1 = s.substr(i0, i - i0);
				s1 = Mytrim(s1);
				if (s1.length() > 0) {
					try {
						char * end;
						double d = std::strtod(s1.c_str(), &end);
						memset(buf2, 0, 128);
						//sprintf_s(buf2, 128, "%f", d);
						sprintf(buf2, "%f", d);
						string s(buf2);
						if (s.compare(s1.c_str()) == 0)
							pVu_std->push_back(d);
						else
							ret = -6;
					}
					catch (std::exception& e) {
						cout << e.what();
						ret = -6;
					}
				}
				else
					ret = -4;
			}
		}
	}
	if (ret == 0)
		return pVu_name->size();
	else
		return ret;
}
int Read_correlation(char* sFileName, int nSize, DoubleMatarix*pMu_cor) 
{
	ifstream _csvInput(sFileName, ios::in);
	if (!_csvInput.is_open())
		return -3;	//The file is not exists
	ClearMatrix(pMu_cor);
	char buf[1024];
	char buf2[128];
	int n = 1;
	int ret = 0;
	while (n > 0) {
		if (ret < 0) {
			cout << "Error occur while reading "<< sFileName<<" at:" << buf;
			break;
		}
		memset(buf, 0, 1024);
		_csvInput.getline(buf, 1024);
		n = strlen(buf);
		int nCol = 0;
		int i, i0;
		if (n > 0) {
			string s(buf);
			i0 = 0;
			DoubleVec stdItems;
			for (int k = 0;k < nSize;k++) {
				if (i0 >= (int)(s.length()))
					break;
				i = s.find(',', i0);	//get std value
				if (i > 0) {
					nCol++;
					string s1 = s.substr(i0, i - i0);
					s1 = Mytrim(s1);
					if (s1.length() > 0) {
						try {
							char * end;
							double d = std::strtod(s1.c_str(), &end);
							memset(buf2, 0, 128);
							sprintf(buf2, "%f", d);
							string s(buf2);
							if (s.compare(s1.c_str()) == 0)
								stdItems.push_back(d);
							else
								ret = -6;
						}
						catch (std::exception& e) {
							cout << e.what();
							ret = -6;
						}
					}
					else {
						ret = -4;
					}
				}
				else {
					//last one 
					i = n;
					nCol++;
					string s1 = s.substr(i0, i - i0);
					s1 = Mytrim(s1);
					if (s1.length() > 0) {			
						try {
							char * end;
							double d = std::strtod(s1.c_str(), &end);
							memset(buf2, 0, 128);
							//sprintf_s(buf2, 128, "%f", d);
							sprintf(buf2, "%f", d);
							string s(buf2);
							if (s.compare(s1.c_str()) == 0)
								stdItems.push_back(d);
							else
								ret = -6;
						}
						catch (std::exception& e) {
							 cout << e.what();
							//cout << e;
							ret = -6;
						}
					}
					else {
						ret = -4;
					}
				}
				i0 = i + 1;
			}
			if(nCol== nSize)
				pMu_cor->push_back(stdItems);
			else
				ret = -4;
		}
	}
	if (ret == 0)
		return pMu_cor->size();
	else
		return ret;
}

