#pragma once
#include "math.h"
#include <iostream> 
#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>

typedef vector<string>StringVec;
typedef vector<float> FloatVec;
typedef vector<FloatVec> FloatMatarix;

typedef vector<double> DoubleVec;
typedef vector<DoubleVec> DoubleMatarix;
typedef vector<string> StrVect;

int ReadAsset(char* sFileName);
void ShowMatrix(DoubleMatarix *pM, int nPrecision = 10);
void ShowVector(DoubleVec *pV, int nPrecision=10 );
void ClearList_Matrix();
void ClearMatrix(DoubleMatarix *pM);
void calu_Rate(DoubleMatarix* pAsset, DoubleMatarix* pRate);
void calu_AvgStd(DoubleMatarix* pRate, DoubleVec* pAvgRate, DoubleVec* pStdRate);
void calu_Covariance(DoubleMatarix* pRate, DoubleVec* pAvgRate, DoubleMatarix* pCov);
void calu_Correlation(DoubleMatarix* pCov, DoubleVec* pStdRate, DoubleMatarix* pCor);

extern StrVect vecColList;
extern DoubleMatarix g_mtrAssets;
extern DoubleMatarix g_mtrRate;
extern DoubleVec g_avg_rate;
extern DoubleVec g_std_rate;
extern DoubleMatarix g_Covariance;
extern DoubleMatarix g_Correlation;

